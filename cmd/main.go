package main

import (
	"net/http"

	"quanxiang.lowcode/faas/api"
)

func main() {
	server := http.Server{Addr: ":3000"}
	http.HandleFunc("/", api.Example)
	server.ListenAndServe()
}
