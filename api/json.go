package api

import (
	"encoding/json"
	"net/http"
)

func Json(w http.ResponseWriter, value interface{}) {
	result, _ := json.Marshal(value)
	w.Header().Set("Content-type", "application/json;charset=utf-8")
	w.Write(result)
}
