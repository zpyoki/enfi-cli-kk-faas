package api

import (
	"net/http"
)

type Request struct {
}

type Response struct {
	Code string  `json:"code"`
	Msg  string  `json:"msg"`
	Data []Datas `json:"data,omitempty"`
}

type Datas struct {
	Id            string  `json:"id"`
	ProName       string  `json:"pro_name"`
	ProCategoryId int     `json:"pro_category_id"`
	CliId         int     `json:"cli_id"`
	AreaSheng     string  `json:"area_sheng"`
	AreaShi       string  `json:"area_shi"`
	StageId       int     `json:"stage_id"`
	QuoteId       int     `json:"quote_id"`
	PredictLimit  float64 `json:"predict_limit"`
	ContSing      string  `json:"cont_sing"`
	Principal     string  `json:"principal"`
}

func Example(w http.ResponseWriter, r *http.Request) {
	db, _ := DbConnect()

	var data []Datas
	db.Table("adm_quota_decision").Select("id", "pro_name", "pro_category_id", "cli_id", "area_sheng", "area_shi", "stage_id",
		"quote_id", "predict_limit", "cont_sing", "principal").Find(&data)
	resp := &Response{"0000", "ok!", data}

	Json(w, resp)
}
