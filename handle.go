package handle

import (
	"net/http"

	"quanxiang.lowcode/faas/api"
)

type Request struct {
}

type Response struct {
	Code string  `json:"code"`
	Msg  string  `json:"msg"`
	Data []Datas `json:"data,omitempty"`
}

type Datas struct {
	Id            string  `json:"id"`
	ProName       string  `json:"pro_name"`
	ProCategoryId int     `json:"pro_category_id"`
	CliId         int     `json:"cli_id"`
	AreaSheng     string  `json:"area_sheng"`
	AreaShi       string  `json:"area_shi"`
	StageId       int     `json:"stage_id"`
	QuoteId       int     `json:"quote_id"`
	PredictLimit  float64 `json:"predict_limit"`
	ContSing      string  `json:"cont_sing"`
	Principal     string  `json:"principal"`
}

// Handler function as a server handler
// @tags handler
// @Summary test6
// @Description handler description
// @Produce json
// @Param request body Request true "request"
// @Success 200 {object} Response
// @Router / [post]
func Handle(w http.ResponseWriter, r *http.Request) {
	api.Example(w, r)
}
